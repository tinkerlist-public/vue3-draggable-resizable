"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DraggableContainer = void 0;
var Vue3DraggableResizable_1 = require("./components/Vue3DraggableResizable");
var DraggableContainer_1 = require("./components/DraggableContainer");
Vue3DraggableResizable_1.default.install = function (app) {
    app.component(Vue3DraggableResizable_1.default.name, Vue3DraggableResizable_1.default);
    app.component(DraggableContainer_1.default.name, DraggableContainer_1.default);
    return app;
};
var DraggableContainer_2 = require("./components/DraggableContainer");
Object.defineProperty(exports, "DraggableContainer", { enumerable: true, get: function () { return DraggableContainer_2.default; } });
exports.default = Vue3DraggableResizable_1.default;
